/**
 * Created by alireza on 7/26/17.
 */
//--------- Text -----------------
let input = document.querySelector('.wrapper .center textarea');
let dirction = document.getElementById("dir");
let bold = document.getElementById("bold");
let italic = document.getElementById("italic");
let fontColor = document.getElementById("color");
let submit = document.getElementById("submit");
let rightMenu = document.querySelectorAll('.wrapper .rightMenu .iconRight');
let searchIcon = rightMenu[0];
let textEditing = rightMenu[1];
let image = rightMenu[2] ;
let searchBox = document.querySelector(".wrapper .rightHide .search");
let textBox = document.querySelector(".wrapper .rightHide .textEditing");
let imageBox = document.querySelector(".wrapper .rightHide .image");
//--------- image ------------------
let fileUpload = document.getElementById("images");




//---------textEvent -----------------
input.addEventListener("change", function () {
    console.log( input.value );
});

dirction.addEventListener("click", function () {
    if ( input.style.direction ==='rtl') {
        input.style.direction = 'ltr';
    } else {
        input.style.direction = 'rtl' ;
    }
});

italic.addEventListener("click", function () {
    if ( input.style.fontStyle !=='italic') {
        input.style.fontStyle = 'italic';
    } else {
        input.style.fontStyle = 'normal' ;
    }
});

bold.addEventListener( "click" , function () {
   if ( input.style.fontWeight !=='bold') {
       input.style.fontWeight = 'bold';
   } else {
       input.style.fontWeight = 'normal' ;
   }
});
submit.addEventListener("click", function () {
    input.style.color = fontColor.value ;
});
/*submit.addEventListener("click",function () {
    input.style.color = fontColor.value ;
});*/


searchIcon.addEventListener('click', function () {
    console.log('a');
    textBox.style.display = 'none';
    imageBox.style.display = 'none';

    textEditing.style.backgroundColor =  '#1D1D1D';
    image.style.backgroundColor = "#1D1D1D";
    searchIcon.style.backgroundColor =  '#fff';
    searchBox.style.display = 'block';

});

textEditing.addEventListener('click' ,function () {
    console.log('a');
    searchBox.style.display = 'none';
    imageBox.style.display = 'none' ;

    textEditing.style.backgroundColor =  '#fff';
    searchIcon.style.backgroundColor = '#1D1D1D';
    image.style.backgroundColor = '#1D1D1D';
    textBox.style.display = 'block';
});

image.addEventListener('click' , function () {
    searchBox.style.display = 'none';
    textBox.style.display = 'none' ;
    imageBox.style.display = 'block' ;
    textEditing.style.backgroundColor = '#1D1D1D' ;
    searchIcon.style.backgroundColor = '#1D1D1D';
    image.style.backgroundColor = '#fff';

});

function findString (str) {
    /* if (parseInt(navigator.appVersion)<4) return;*/
    let strFound;
    if (window.find) {

        // CODE FOR BROWSERS THAT SUPPORT window.find

        strFound = self.find(str);
        if (!strFound) {
            strFound = self.find(str, 0, 1);
            while (self.find(str, 0, 1)) continue;
        }
    }
}


//---------------imageEvent ---------------------

fileUpload.addEventListener('change' ,function () {
    let file = document.getElementById("images").files[0];
    let reader = new FileReader();
    reader.addEventListener("load",function () {
        document.querySelector('#textArea').style.backgroundImage = "url(" + reader.result + ")";
        console.log(reader);
    });

    if(file){
        reader.readAsDataURL(file);
    }else{
    }
},true);




















